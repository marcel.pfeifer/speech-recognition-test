let btn = document.getElementById("start");
btn.onclick = () => {
    recognition.start();
    console.log('Bereit für eine Aufforderung.');
}

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

let commands = ['zeitung', 'hallo', 'leben']

var grammar = '#JSGF V1.0; grammar commands; public <commands> = ' + commands.join(' | ') + ' ;'


var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
recognition.continuous = false;
recognition.lang = 'de-DE';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');

recognition.onresult = function (event) {
    let result = event.results[0][0];
    let command = findCommand(result.transcript);

    if (command) {
        diagnostic.textContent = 'Befehl erkannt: ' + command + ' Mit einer Sicherheit von: ' + Math.floor(result.confidence * 100) + '% erkannt.';
        bg.style.backgroundColor = "green";
    } else {
        diagnostic.textContent = 'Kein Befehl erkannt.';
        bg.style.backgroundColor = "red";
    }
   
    console.log('Confidence: ' + result.confidence);
}

recognition.onspeechend = function () {
    recognition.stop();
}

recognition.onnomatch = function (event) {
    diagnostic.textContent = "Ich habe die Aufforderung nicht erkannt.";
}

recognition.onerror = function (event) {
    diagnostic.textContent = 'Error' + event.error;
}

function findCommand(str) {
    return commands.find(command => {
        return str.toLowerCase().includes(command);
    })
}